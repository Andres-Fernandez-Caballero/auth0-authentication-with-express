const configs = require('./src/configs');
const http = require('http');

const app = require('./src/app');

http.createServer(app).listen(configs.port, () => {
    console.log(`Server is running on port ${configs.port}`);
});
const express = require('express');
const routes = require('./routes');
const { auth } = require('express-openid-connect');
const configs = require('./configs');

const app = express();

app.use(auth(configs.auth0));

app.use(function (req, res, next) {
    res.locals.user = req.oidc.user;
    next();
});

app.use(routes)

module.exports = app;
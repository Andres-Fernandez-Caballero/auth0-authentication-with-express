require('dotenv').config();

const { PORT } = process.env;
const { AUTH0_BASE_APP_URL, AUTH0_SECRET, AUTH0_ISSUER_BASE_URL, AUTH0_CLIENT_ID } = process.env;

const configs = {
    port: PORT ?? 3000,

    auth0: {
        authRequired: false,
        auth0Logout: true,
        baseURL: AUTH0_BASE_APP_URL,
        secret: AUTH0_SECRET,
        issuerBaseURL: AUTH0_ISSUER_BASE_URL,
        clientID: AUTH0_CLIENT_ID,
    }
};

module.exports = configs;
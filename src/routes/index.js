const { Router } = require('express');
const { requiresAuth } = require('express-openid-connect');

const serverRouter = Router();

serverRouter.get('/', (req, res) => {
    res.send(req.oidc.isAuthenticated() ? 'Logged in' : 'Logged out');
})

serverRouter.get('/home', requiresAuth(), (req, res) => {
    res.json({ user: JSON.stringify(req.oidc.user, null, 2), });
})



serverRouter.get("/*", (req, res) => {
    res.status(404).send("404 Not Found");
});

module.exports = serverRouter;